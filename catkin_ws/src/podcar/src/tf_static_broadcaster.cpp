#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include "geometry_msgs/Vector3.h"

/**
 * Broadcasting the static TF Transforms
 */
int main(int argc, char** argv){
    ros::init(argc, argv, "tf_static_broadcaster");

    ros::NodeHandle node;

    ros::Rate r(100);

    tf::TransformBroadcaster broadcaster;

    tf::Quaternion wheel_rotation;
    wheel_rotation.setEuler(0, 1.5708, 0);

    while(node.ok()){
        broadcaster.sendTransform(
                tf::StampedTransform(
                        tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0, 0, 0)),
                        ros::Time::now(),"base_link", "podcar_body"));
        broadcaster.sendTransform(
                tf::StampedTransform(
                        tf::Transform(wheel_rotation, tf::Vector3(-0.35001, -0.4,  0.1)),
                        ros::Time::now(),"podcar_body", "wheel_front_right"));
        broadcaster.sendTransform(
                tf::StampedTransform(
                        tf::Transform(wheel_rotation, tf::Vector3(0.35001, -0.4,  0.1)),
                        ros::Time::now(),"podcar_body", "wheel_front_left"));
        broadcaster.sendTransform(
                tf::StampedTransform(
                        tf::Transform(wheel_rotation, tf::Vector3(-0.35001, 0.4,  0.1)),
                        ros::Time::now(),"podcar_body", "wheel_rear_right"));
        broadcaster.sendTransform(
                tf::StampedTransform(
                        tf::Transform(wheel_rotation, tf::Vector3(0.35001, 0.4,  0.1)),
                        ros::Time::now(),"podcar_body", "wheel_rear_left"));
        broadcaster.sendTransform(
                tf::StampedTransform(
                        tf::Transform(tf::Quaternion(0, 0, 1, 1), tf::Vector3(-0.23, 0, 1.4)),
                        ros::Time::now(),"podcar_body", "my_velodyne_base"));
        broadcaster.sendTransform(
                tf::StampedTransform(
                        tf::Transform(tf::Quaternion(0, 0, 0, 1), tf::Vector3(0, 0, 0)),
                        ros::Time::now(),"my_velodyne_base", "my_velodyne_top"));
        r.sleep();
    }

    return 0;
};
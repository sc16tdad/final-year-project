#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include "sensor_msgs/PointCloud.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Pose.h"

/**
 * Broadcasting the dynamic TF Transforms
 */


/// \brief Variables for knowing the rotation angle.
float angle;
float speed;

/**
 * Transforming the point cloud data to the given coordinates
 * @param _info The data received from the topic with the point clouds
 */
void pointCloudInfo(const sensor_msgs::PointCloud::ConstPtr &_info) {
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    tf::Quaternion q;

    q.setRPY(0, 0, angle);

    transform.setOrigin( tf::Vector3(_info->points[0].x, _info->points[0].y, _info->points[0].z) );
    transform.setRotation(q);

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "point_cloud_link"));

    angle += speed; // increase angle of rotation
    if (angle > 6.28319) { // equivalent to a full turn, reset angle to zero
        angle = 0;
    }
}

/**
 * Transforming the podcar's pose, to reflect where it has moved to
 * @param _info The data received from the topic with the odometry
 */
void poseInfo(const nav_msgs::Odometry::ConstPtr &_info) {
    static tf::TransformBroadcaster br;
    tf::Transform transform;

    geometry_msgs::Pose pose = _info->pose.pose;

    tf::Quaternion q (pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w);

    transform.setOrigin( tf::Vector3(pose.position.x, pose.position.y, pose.position.z) );
    transform.setRotation(q);

    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "base_link"));
}


int main(int argc, char** argv){
    ros::init(argc, argv, "tf_topic_broadcaster");

    ros::NodeHandle node;

    angle = 0;
    speed = 0.06904604395; // best value I got, for when the Velodyne's speed is 2
    ros::Subscriber sub1 = node.subscribe("/point_cloud", 10, &pointCloudInfo);
    ros::Subscriber sub2 = node.subscribe("/my_odom", 10, &poseInfo);

    ros::spin();
    return 0;
};


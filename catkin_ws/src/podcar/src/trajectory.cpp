#include "ros/ros.h"
#include "geometry_msgs/Twist.h"

using namespace std;

/**
 * Sending Twist messages to a ROS topic, for robot to follow specified trajectories
 */
int main(int argc, char **argv)
{
    int commands = 0; // variable to know how many commands there are in the chosen trajectory

    vector< vector<float> > trajectory; // the trajectory, contains commands
    vector<float> command; // will contain 7 elements; seconds, linear.x, linear.y, linear.z, angular.x, angular.y, angular.z

    int YorN;
    cout << "Would you like to use an existing trajectory? (1 for yes, 0 for no)" << endl;
    cin >> YorN;

    if (YorN == 0) { // if user chooses to give their own trajectory
        cout << "Please specify how many commands you want to give" << endl;
        cin >> commands;

        cout << "Please give the values as it asks" << endl;
        for (int i = 0; i < commands; i++) {
            cout << "----- Command number: " << i + 1 << " -----" << endl;

            float add = 0;

            cout << "seconds: ";
            cin >> add;
            command.push_back(add * 10); // times by 10, as the sleep rate is 10Hz, which is 0.1 seconds, need to multiply the second by 10 to make it milliseconds

            cout << "linear.x: ";
            cin >> add;
            command.push_back(add);

            cout << "linear.y: ";
            cin >> add;
            command.push_back(add);

            cout << "linear.z: ";
            cin >> add;
            command.push_back(add);

            cout << "angular.x: ";
            cin >> add;
            command.push_back(add);

            cout << "angular.y: ";
            cin >> add;
            command.push_back(add);

            cout << "angular.z: ";
            cin >> add;
            command.push_back(add);

            trajectory.push_back(command);
            command.clear();
        }

    } else { // if the user chooses to use a default trajectory

        int traj;
        cout << "Which trajectory would you like? 1 (default), 2, 3, 4 or 5?" << endl;
        cin >> traj;

        if (traj == 5) {
            commands = 5;

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 2); // adding 1.5 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 130); // adding 70 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            // trajectory6
            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(-1.4); // adding -1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 30); // adding 30 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 2); // adding 2 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 160); // adding 120 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(-1.5); // adding -1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 25); // adding 25 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 1.5); // adding 2 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 130); // adding 120 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

        } else if (traj == 4) {
            commands = 4;

            // trajectory4
            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(-1.5); // adding -1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 30); // adding 30 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 1.5); // adding 1.5 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 70); // adding 70 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(-1.5); // adding -1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 25); // adding 25 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 2); // adding 2 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 120); // adding 120 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();
        } else if (traj == 3) {
            commands = 6;

            // trajectory3
            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(1.5); // adding 1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 30); // adding 30 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 1.5); // adding 1.5 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 50); // adding 50 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(-1.5); // adding -1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 25); // adding 25 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 2); // adding 2 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 100); // adding 100 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(-1.5); // adding -1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 25); // adding 25 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 1.8); // adding 1.8 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 80); // adding 80 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();
        } else if (traj == 2) {
            commands = 3;

            // trajectory2
            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 1.5); // adding 1.5 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 50); // adding 50 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.push_back(1.5); // adding 1.5 as the last value of the vector (angular.z)
            command.insert(command.begin(), 20); // adding 20 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();

            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 1.3); // adding 1.3 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 30); // adding 30 as the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();
        } else {
            commands = 1;

            // trajectory1
            command.assign(5, 0); // adding 5 zeros to the vector
            command.insert(command.begin(), 1.5); // adding 1.5 as the first value of the vector (linear.x value)
            command.insert(command.begin(), 100); // adding 100 to the first value of the vector (amount of milliseconds for this command to take place)

            trajectory.push_back(command);
            command.clear();
        }
    }

    ros::init(argc, argv, "trajectory");

    ros::NodeHandle n;

    ros::Publisher teleop_pub = n.advertise<geometry_msgs::Twist>("teleop/cmd_vel", 1000); // will publish these values to the teleop topic

    ros::Rate loop_rate(10); // every 0.1 seconds

    // initialising variables for logic
    int curCommand = 0; // the current command
    int steps = trajectory[curCommand][0]; // how many steps there are in the current command - how many seconds it will be running for
    int count = 1; // counting how many steps have occurred

    ROS_INFO("Starting trajectory...");

    while (ros::ok())
    {
        geometry_msgs::Twist data;

        if (count <= steps) {
            data.linear.x = trajectory[curCommand][1];
            data.linear.y = trajectory[curCommand][2];
            data.linear.z = trajectory[curCommand][3];

            data.angular.x = trajectory[curCommand][4];
            data.angular.y = trajectory[curCommand][5];
            data.angular.z = trajectory[curCommand][6];

            if (count == steps && curCommand < commands - 1) { // if the current command is finished
                curCommand++;
                steps += trajectory[curCommand][0];
            }

        } else {
            data.linear.x = 0;
            data.linear.y = 0;
            data.linear.z = 0;

            data.angular.x = 0;
            data.angular.y = 0;
            data.angular.z = 0;
        }

        teleop_pub.publish(data); // publishing the data

        ros::spinOnce();

        loop_rate.sleep();
        ++count;
    }

    ROS_INFO("End of trajectory!");
    return 0;
}
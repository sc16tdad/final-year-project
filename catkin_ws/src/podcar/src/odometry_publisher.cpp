#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <gazebo_msgs/GetModelState.h>

/**
 * Class created to publish the odometry data of the podcar to a ROS topic
 * Based on The Construct ROS Q&A 007 - How to publish odometry from simulation position
 * https://www.youtube.com/watch?v=I_5leJK8vhQ&list=PLWQ0bLU1DUcnvyGfQ_dvVxE7MzFzs6k67&index=12&t=185s
 * Accessed on: April 2020
 */
int main(int argc, char** argv){
    ros::init(argc, argv, "odom_pub");

    ros::NodeHandle node;
    ros::Publisher pub = node.advertise<nav_msgs::Odometry>("/my_odom", 1000);

    ros::ServiceClient get_model_state = node.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");

    nav_msgs::Odometry odom;
    odom.child_frame_id = "base_link";
    odom.header.frame_id = "/odom";

    gazebo_msgs::GetModelState model_state;
    model_state.request.model_name = "podcar";


    ros::Rate r(10);
    while(node.ok()){

        if (get_model_state.call(model_state)) {

            odom.pose.pose = model_state.response.pose;
            odom.twist.twist = model_state.response.twist;
            odom.header.stamp = ros::Time::now();

            pub.publish(odom);
        }

        r.sleep();
    }

    return 0;
};
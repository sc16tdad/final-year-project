#ifndef _TELEOP_PLUGIN_HH_
#define _TELEOP_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Vector3.h"


/**
 * Class created to give motion to the robot simulation
 * Based on Gazebo tutorial: Intermediate: Control plugin - http://gazebosim.org/tutorials?tut=guided_i5
 * Accessed on: February 2020
 */
namespace gazebo {
    /// \brief A plugin to control the Teleoperation of the Podcar.
    class TeleopPlugin : public ModelPlugin {

        /// \brief Constructor
        public: TeleopPlugin() {}

        /// \brief The load function is called by Gazebo when the plugin is
        /// inserted into simulation
        /// \param[in] _model A pointer to the model that this plugin is
        /// attached to.
        /// \param[in] _sdf A pointer to the plugin's SDF element.
        public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
            // Safety check
            if (_model->GetJointCount() == 0) {
                std::cerr << "Invalid joint count, Teleop plugin not loaded\n";
                return;
            }

            // Store the model pointer for convenience.
            this->model = _model;

            // Get the joints for each wheel.
            this->jointRearLeft = _model->GetJoint("wheel_rear_left_joint");
            this->jointRearRight = _model->GetJoint("wheel_rear_right_joint");
            this->jointFrontLeft = _model->GetJoint("wheel_front_left_joint");
            this->jointFrontRight = _model->GetJoint("wheel_front_right_joint");

            // Create the node
            this->node = transport::NodePtr(new transport::Node());
            #if GAZEBO_MAJOR_VERSION < 8
            this->node->Init(this->model->GetWorld()->GetName());
            #else
            this->node->Init(this->model->GetWorld()->Name());
            #endif

            // Create a topic name to listen to
            std::string topicName = "/teleop/cmd_vel";

            // Initialize ros, if it has not already been initialized.
            if (!ros::isInitialized())
            {
                int argc = 0;
                char **argv = NULL;
                ros::init(argc, argv, "gazebo_client",
                          ros::init_options::NoSigintHandler);
            }

            // Create our ROS node. This acts in a similar manner to
            // the Gazebo node
            this->rosNode.reset(new ros::NodeHandle("gazebo_client"));

            // Create the named topic, and subscribe to it.
            ros::SubscribeOptions so =
                    ros::SubscribeOptions::create<geometry_msgs::Twist>(
                            topicName,
                            1,
                            boost::bind(&TeleopPlugin::OnRosMsg, this, _1),
                            ros::VoidPtr(), &this->rosQueue);
            this->rosSub = this->rosNode->subscribe(so);

            // Spin up the queue helper thread.
            this->rosQueueThread =
                    std::thread(std::bind(&TeleopPlugin::QueueThread, this));
        }

        /// \brief Set the velocity of the Teleop
        /// \param[in] _vel New target velocity
        public: void SetVelocity(const geometry_msgs::Twist::ConstPtr &_vel) {
            // Allowing for angular movement, applying this for the whole model
            if (_vel->angular.z != 0) {
                this->model->SetAngularVel(ignition::math::Vector3d(_vel->angular.x, _vel->angular.y, _vel->angular.z));
            }

            // Set the joints' target linear velocities.
            this->jointRearLeft->SetVelocity(0, _vel->linear.x);
            this->jointRearRight->SetVelocity(0, _vel->linear.x);
            this->jointFrontLeft->SetVelocity(0, _vel->linear.x);
            this->jointFrontRight->SetVelocity(0, _vel->linear.x);
        }

        /// \brief Pointer to the model.
        private: physics::ModelPtr model;

        /// \brief Pointer to the joint.
        private: physics::JointPtr jointRearLeft;

            /// \brief Pointer to the joint.
        private: physics::JointPtr jointRearRight;

            /// \brief Pointer to the joint.
        private: physics::JointPtr jointFrontRight;

            /// \brief Pointer to the joint.
        private: physics::JointPtr jointFrontLeft;

        /// \brief A PID controller for the joint.
        private: common::PID pid;

        /// \brief A node used for transport
        private: transport::NodePtr node;

        /// \brief A subscriber to a named topic.
        private: transport::SubscriberPtr sub;

        /// \brief A node use for ROS transport
        private: std::unique_ptr<ros::NodeHandle> rosNode;

        /// \brief A ROS subscriber
        private: ros::Subscriber rosSub;

        /// \brief A ROS callbackqueue that helps process messages
        private: ros::CallbackQueue rosQueue;

        /// \brief A thread the keeps running the rosQueue
        private: std::thread rosQueueThread;

        /// \brief Handle an incoming message from ROS
        /// \param[in] _msg A float value that is used to set the velocity
        /// of the Teleop.
        public: void OnRosMsg(const geometry_msgs::Twist::ConstPtr &_msg)
        {
            this->SetVelocity(_msg);
        }

        /// \brief ROS helper function that processes messages
        private: void QueueThread()
        {
            static const double timeout = 0.01;
            while (this->rosNode->ok())
            {
                this->rosQueue.callAvailable(ros::WallDuration(timeout));
            }
        }
    };

    // Tell Gazebo about this plugin, so that Gazebo can call Load on this plugin.
    GZ_REGISTER_MODEL_PLUGIN(TeleopPlugin)
}
#endif
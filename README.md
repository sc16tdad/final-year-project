# Final Year Project - COMP3931

This code was completed in Ubuntu 16.04, using ROS Kinetic.

# Compile code
1. Download the repository
2. Open a shell
3. Go into the final-year-project/catkin_ws folder
4. Run `catkin_make`
4. Source the setup.bash by running `. devel/setup.bash`

Make sure that for every shell opened, setup.bash has been run (else ROS won't find these packages)

# Launch the Podcar Package
1. Run `roslaunch podcar podcar_model.launch`

# Open RViz
There are two options for this:
1. Run `rosrun rviz rviz` and then setup own configurations
2. Run `rosrun rviz rviz -d <filename>`, where &lt;filename&gt; should be replaced by an RViz configuration file.
For the second option, within final-year-project/catkin_ws/src/podcar/config there are files with ".rviz" extension that can be used.

# Change speed of Velodyne
Simply run `rostopic pub /velodyne/vel_cmd std_msgs/Float32 5`, where 5 can be replaced by any number (this is the velocity you are setting)

# Move the Podcar
If not already done, install the teleop-twist-keyboard package: http://wiki.ros.org/teleop_twist_keyboard
This can be done through the following command (on Ubuntu machines):
`sudo apt-get install ros-kinetic-teleop-twist-keyboard`

Then run the following command and follow the instructions given in the shell:
`rosrun teleop_twist_keyboard teleop_twist_keyboard.py cmd_vel:=/teleop/cmd_vel`

